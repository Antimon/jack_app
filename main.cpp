/*
 *   License:  GPLv3
 *
 *   jack_app is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   jack_app is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with jack_app.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>
#include <jack/jack.h>
#include <vector>
#include <sndfile.h>
#include <string>
#include <thread>
#include <sys/stat.h>
#include <dirent.h>
#include <fftw3.h>
#include <iostream>

using namespace std;

/*************** Compiling Options ***************/

// Doing the multiplying in n threads. Defined for n = 1, 2 or 4
#define NUMBER_OF_MULTIPLYING_THREADS 1

// Doing all the calculation for each input-steam in own thread
#define MASSIVE_MULTITHREADING true

// If compiled with 'true', the application will only support two input-channels (left and right) and
// will only use two IRs
#define ONLY_DO_STEREO false

// Finally, all arrays must be added up to stereo signal.
// If 'true' this programm will do that in two threads
#define ADD_FINALLY_ADD_ARRAYS_IN_THREADS true


/*************** Global Variables ***************/

typedef struct{

    SF_INFO info;                   // Soundfile header-information
    SNDFILE* soundfile = NULL;      // Soundfile-representation
    float* IR_samples = NULL;       // Impulse-response in time-domain
    fftw_complex* IR_image = NULL;  // Impulse-response in image-domain

    // On different places in this program we need the information 'buffersize + in_IR->info.frames - 1'. Calculating that once,
    // because we assume buffersize to be constant anyway.
    uint32_t size_ExtendedArrays = -1;

    float* toAddBuffer = NULL;      // Values to add ('Overlapp-Add')
    uint32_t size_toAddBuffer = -1;


    // All IRs might have a different length, we need different arrays for working with the data. To avoid allocating them multiply times,
    // we put them into this struct and re-use them...

    fftw_complex* IR_samples_complex = NULL;    // Input-array for fft

    fftw_complex* mult_result_time = NULL;      // Will contain temporary result of multiplication of the two arrays (in image-domain) but already moved back to time-domain.

    fftw_complex* extendedBuffer_time = NULL;   // Will contain the current buffer filled up with zeros

    fftw_complex* extendedBuffer_image = NULL;  // Will contain the current buffer moved to image-domain

    fftw_complex* mult_result_image = NULL;     // Will contain the result of the multiplicated buffers in image-domain

    // Needed plans for fftw
    fftw_plan plan_fourierIR = NULL;
    fftw_plan plan_fromImageToTimeDomain = NULL;
    fftw_plan plan_fourierBuffer = NULL;

}IR;

#if ONLY_DO_STEREO == false
jack_port_t* input_center;
jack_port_t* input_frontLeft;
jack_port_t* input_frontRight;
jack_port_t* input_surroundLeft;
jack_port_t* input_surroundRight;
#else
jack_port_t* input_left;
jack_port_t* input_right;
#endif // ONLY_DO_STEREO

jack_port_t* output_left;
jack_port_t* output_right;

uint32_t buffersize = -1;   //we assume, the buffersize is always constant.

#if ONLY_DO_STEREO == false
IR IRcenter_leftEar;
IR IRcenter_rightEar;
IR IRfrontLeft_leftEar;
IR IRfrontLeft_rightEar;
IR IRfrontRight_leftEar;
IR IRfrontRight_rightEar;
IR IRsurroundLeft_leftEar;
IR IRsurroundLeft_rightEar;
IR IRsurroundRight_leftEar;
IR IRsurroundRight_rightEar;
#else
IR IR_leftEar;
IR IR_rightEar;
#endif // ONLY_DO_STEREO

#if ONLY_DO_STEREO == false
string pathname = "./IRs_dolby";
#else
string pathname = "./IRs_stereo";
#endif // ONLY_DO_STEREO

// This arrays will be used in processCallbackFunction() to store the results of the calculations
#if ONLY_DO_STEREO == false
float* result_center_leftEar = NULL;
float* result_center_rightEar = NULL;
float* result_frontLeft_leftEar = NULL;
float* result_frontLeft_rightEar = NULL;
float* result_frontRight_leftEar = NULL;
float* result_frontRight_rightEar = NULL;
float* result_surroundLeft_leftEar = NULL;
float* result_surroundLeft_rightEar = NULL;
float* result_surroundRight_leftEar = NULL;
float* result_surroundRight_rightEar = NULL;
#else
float* result_leftEar = NULL;
float* result_rightEar = NULL;
#endif // ONLY_DO_STEREO

// variables for dynamic scaling.
float scalingFactor = 1;
float delta_ScalingFactor = 0.0001; //after clipping, the scalingFactor will be decreased by this delta
float headroom = 0.1;               // but we keep this headroom


// Will be called if buffersize changes, which can not be handled by
// this application.
void buffsize_callback(jack_nframes_t nframes, void *arg){
    cout << "Buffsize_callback() called. " << endl;
    cout << "Old buffersize: " << buffersize << endl;
    cout << "New buffersize: " << nframes << endl;
    if( buffersize != nframes ){
        cout << "Exit now!" << endl;
        exit(1);
    }
}

void xrun_callback(){
    cout << "got x-run. Maybe use shorter IRs?" << endl;
}

int getFilesInDirectory( vector<string>* in_files ){
    DIR *dp;
    struct dirent *dirp;
    if((dp  = opendir( pathname.c_str() )) == NULL) {
        cout << "Error(" << errno << ") opening " << pathname << endl;
        return errno;
    }
    while ((dirp = readdir(dp)) != NULL) {
        (*in_files).push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}

void removePointsFromArray( vector<string> &VectorWithFiles ){
    int positionOfOnePointInVector = -1;
    int positionOfTwoPointsInVector = -1;
    for( size_t index = 0; index < VectorWithFiles.size(); index++ ){
        if( VectorWithFiles[index] == "." ){
            positionOfOnePointInVector = index;
        }
    }
    VectorWithFiles.erase( VectorWithFiles.begin() + positionOfOnePointInVector );
    for( size_t index = 0; index < VectorWithFiles.size(); index++ ){
        if( VectorWithFiles[index] == ".." ){
            positionOfTwoPointsInVector = index;
        }
    }
    VectorWithFiles.erase( VectorWithFiles.begin() + positionOfTwoPointsInVector );
}

// Shutdown callback for this JACK application. It is called by JACK if the server ever shuts down or decides to disconnect the client.
void jack_shutdown(void *arg){
    cout << "jack_shutdown() called. Exit now!" << endl;
    exit (0);
}

void validate( vector<string>* in_toValidateStrings ){
    #if ONLY_DO_STEREO == false
    bool center_leftEar = false;
    bool center_rightEar = false;
    bool frontLeft_leftEar = false;
    bool frontLeft_rightEar = false;
    bool frontRight_leftEar = false;
    bool frontRight_rightEar = false;
    bool surroundLeft_leftEar = false;
    bool surroundLeft_rightEar = false;
    bool surroundRight_leftEar = false;
    bool surroundRight_rightEar = false;
    #else
    bool leftEar = false;
    bool rightEar = false;
    #endif // ONLY_DO_STEREO

    #if ONLY_DO_STEREO == false
    for( size_t index = 0; index < (*in_toValidateStrings).size(); index++ ){
        if( (*in_toValidateStrings)[index] == "center_leftEar.wav" ){
            center_leftEar = true;
        }else if( (*in_toValidateStrings)[index] == "center_rightEar.wav" ){
            center_rightEar = true;
        }else if( (*in_toValidateStrings)[index] == "frontLeft_leftEar.wav" ){
            frontLeft_leftEar = true;
        }else if( (*in_toValidateStrings)[index] == "frontLeft_rightEar.wav" ){
            frontLeft_rightEar = true;
        }else if( (*in_toValidateStrings)[index] == "frontRight_leftEar.wav" ){
            frontRight_leftEar = true;
        }else if( (*in_toValidateStrings)[index] == "frontRight_rightEar.wav" ){
            frontRight_rightEar = true;
        }else if( (*in_toValidateStrings)[index] == "surroundLeft_leftEar.wav" ){
            surroundLeft_leftEar = true;
        }else if( (*in_toValidateStrings)[index] == "surroundLeft_rightEar.wav" ){
            surroundLeft_rightEar = true;
        }else if( (*in_toValidateStrings)[index] == "surroundRight_leftEar.wav" ){
            surroundRight_leftEar = true;
        }else if( (*in_toValidateStrings)[index] == "surroundRight_rightEar.wav" ){
            surroundRight_rightEar = true;
        }else{
            cout << "There are files in the IR-folder, which shouldn't be there." << endl;
            cout << "Exit now!" << endl;
            exit(1);
        }
    }
    #else
    for( size_t index = 0; index < (*in_toValidateStrings).size(); index++ ){
        if( (*in_toValidateStrings)[index] == "leftEar.wav" ){
            leftEar = true;
        }else if( (*in_toValidateStrings)[index] == "rightEar.wav" ){
            rightEar = true;
        }else{
            cout << "There are files in the IR-folder, which shouldn't be there." << endl;
            cout << "Remember: you compiling for stereo-input. You need exactly two IRs. " << endl;
            cout << "One 'leftEar.wav', the other 'rightEar.wav'. " << endl;
            cout << "Both in the folder '" << pathname << "'." << endl;
            cout << "exit now!" << endl;
            exit(1);
        }
    }
    #endif // ONLY_DO_STEREO

    if(
        #if ONLY_DO_STEREO == false
        !center_leftEar ||
        !center_rightEar ||
        !frontLeft_leftEar ||
        !frontLeft_rightEar ||
        !frontRight_leftEar ||
        !frontRight_rightEar ||
        !surroundLeft_leftEar ||
        !surroundLeft_rightEar ||
        !surroundRight_leftEar ||
        !surroundRight_rightEar
        #else
        !leftEar ||
        !rightEar
        #endif // ONLY_DO_STEREO
                                        ){

        cout << "At least on file is not at the right place. Exit now!" << endl;
        exit(1);
    }
}

void createSoundFile( string in_pathToFile, IR* in_IR ){
    in_IR->soundfile = sf_open( in_pathToFile.c_str(), SFM_READ, &(in_IR->info) );
}

void scaleArray_float( float* in_array, uint32_t in_lengthOfArray, float in_factor ){
    for( size_t index = 0; index < in_lengthOfArray; index++){
        in_array[index] = in_array[index] * in_factor;
    }
}

void generateSoundfileHandles(){
    struct stat info;
    if( stat( pathname.c_str(), &info ) != 0 ){
        cout << "cannot access " << pathname << endl;
        exit(1);
    }
    else if( info.st_mode & S_IFDIR ){
        cout << pathname << " is a directory" << endl;
    }
    else{
        cout << pathname << " is no directory\n" << endl;
        exit(1);
    }

    vector<string> stringsToFiles;
    getFilesInDirectory( &stringsToFiles );
    removePointsFromArray(stringsToFiles);
    validate( &stringsToFiles );            //checking for missing files

    #if ONLY_DO_STEREO == false
    if( stringsToFiles.size() != 10 ){
        cout << "Directory with IRs MUST contain 10 files. " << endl;
        cout << "Not 9, not 11. Five is right out. " << endl;
        cout << "Exit now!" << endl;
        exit(1);
    }
    #else
    if( stringsToFiles.size() != 2 ){
        cout << "Directory with IRs MUST contain 2 files. " << endl;
        cout << "not 1, not 3. Five is right out. " << endl;
        cout << "Exit now!" << endl;
        exit(1);
    }
    #endif // ONLY_DO_STEREO

    #if ONLY_DO_STEREO == false
    createSoundFile( pathname + "/center_leftEar.wav", &IRcenter_leftEar );
    createSoundFile( pathname + "/center_rightEar.wav", &IRcenter_rightEar );

    createSoundFile( pathname + "/frontLeft_leftEar.wav", &IRfrontLeft_leftEar );
    createSoundFile( pathname + "/frontLeft_rightEar.wav", &IRfrontLeft_rightEar );

    createSoundFile( pathname + "/frontRight_leftEar.wav", &IRfrontRight_leftEar );
    createSoundFile( pathname + "/frontRight_rightEar.wav", &IRfrontRight_rightEar );

    createSoundFile( pathname + "/surroundLeft_leftEar.wav", &IRsurroundLeft_leftEar );
    createSoundFile( pathname + "/surroundLeft_rightEar.wav", &IRsurroundLeft_rightEar );

    createSoundFile( pathname + "/surroundRight_leftEar.wav", &IRsurroundRight_leftEar );
    createSoundFile( pathname + "/surroundRight_rightEar.wav", &IRsurroundRight_rightEar );
    #else
    createSoundFile( pathname + "/leftEar.wav", &IR_leftEar );
    createSoundFile( pathname + "/rightEar.wav", &IR_rightEar );
    #endif // ONLY_DO_STEREO

    if(
            #if ONLY_DO_STEREO == false
            IRcenter_leftEar.info.channels != 1 ||
            IRcenter_rightEar.info.channels != 1 ||

            IRfrontLeft_leftEar.info.channels != 1 ||
            IRfrontLeft_rightEar.info.channels != 1 ||

            IRfrontRight_leftEar.info.channels != 1 ||
            IRfrontRight_rightEar.info.channels != 1 ||

            IRsurroundLeft_leftEar.info.channels != 1 ||
            IRsurroundLeft_rightEar.info.channels != 1 ||

            IRsurroundRight_leftEar.info.channels != 1 ||
            IRsurroundRight_rightEar.info.channels != 1
            #else
            IR_leftEar.info.channels != 1 ||
            IR_rightEar.info.channels != 1
            #endif // ONLY_DO_STEREO
                                                            ){

        cout << "One or more files are not mono (or no proper wav-file). Exit now!" << endl;
        exit(1);
    }
    cout << "SoundFiles generated." << endl;
}

void fourierIR( IR* in_IR ){

    for( uint32_t index = 0; index < in_IR->info.frames; index++ ){
        in_IR->IR_samples_complex[index][0] = (double)( in_IR->IR_samples[index] );
    }
    fftw_execute(in_IR->plan_fourierIR);
}

void initArrays( IR* in_IR ){

    in_IR->IR_samples = new float[ in_IR->size_ExtendedArrays ];
    in_IR->IR_image = (fftw_complex*)fftw_malloc( sizeof(fftw_complex)*(in_IR->size_ExtendedArrays) );
    in_IR->size_toAddBuffer = in_IR->info.frames - 1;
    in_IR->toAddBuffer = new float[ in_IR->size_toAddBuffer ];

    // Attention: Because that array has a different size then the others, i can not init it the same way
    memset( in_IR->toAddBuffer, 0, (in_IR->size_toAddBuffer)*sizeof(float) );

    in_IR->IR_samples_complex = (fftw_complex*)fftw_malloc( sizeof(fftw_complex)*(in_IR->size_ExtendedArrays) );
    in_IR->mult_result_time = (fftw_complex*)fftw_malloc( sizeof(fftw_complex)*(in_IR->size_ExtendedArrays) );
    in_IR->extendedBuffer_time = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * (in_IR->size_ExtendedArrays ));
    in_IR->extendedBuffer_image = (fftw_complex*)fftw_malloc( sizeof(fftw_complex)*(in_IR->size_ExtendedArrays) );
    in_IR->mult_result_image = (fftw_complex*)fftw_malloc( sizeof(fftw_complex)*in_IR->size_ExtendedArrays );

    // init them
    for( size_t index = 0; index < in_IR->size_ExtendedArrays; index++ ){
        in_IR->IR_samples[index] = 0;

        in_IR->IR_image[index][0] = 0;
        in_IR->IR_image[index][1] = 0;

        in_IR->IR_samples_complex[index][0] = 0;
        in_IR->IR_samples_complex[index][1] = 0;

        in_IR->mult_result_time[index][0] = 0;
        in_IR->mult_result_time[index][1] = 0;

        in_IR->extendedBuffer_time[index][0] = 0;
        in_IR->extendedBuffer_time[index][1] = 0;

        in_IR->extendedBuffer_image[index][0] = 0;
        in_IR->extendedBuffer_image[index][1] = 0;

        in_IR->mult_result_image[index][0] = 0;
        in_IR->mult_result_image[index][1] = 0;

    }
    sf_read_float( in_IR->soundfile, in_IR->IR_samples, in_IR->info.frames );
}

void init_resultArrays(){
    #if ONLY_DO_STEREO == false
    result_center_leftEar = new float[ buffersize ];
    result_center_rightEar = new float[ buffersize ];
    result_frontLeft_leftEar = new float[ buffersize ];
    result_frontLeft_rightEar = new float[ buffersize ];
    result_frontRight_leftEar = new float[ buffersize ];
    result_frontRight_rightEar = new float[ buffersize ];
    result_surroundLeft_leftEar = new float[ buffersize ];
    result_surroundLeft_rightEar = new float[ buffersize ];
    result_surroundRight_leftEar = new float[ buffersize ];
    result_surroundRight_rightEar = new float[ buffersize ];
    #else
    result_leftEar = new float[ buffersize ];
    result_rightEar = new float[ buffersize ];
    #endif // ONLY_DO_STEREO

    // better to init them as well:
    #if ONLY_DO_STEREO == false
    for( size_t index = 0; index < buffersize; index++ ){
        result_center_leftEar[index] = 0;
        result_center_rightEar[index] = 0;
        result_frontLeft_leftEar[index] = 0;
        result_frontLeft_rightEar[index] = 0;
        result_frontRight_leftEar[index] = 0;
        result_frontRight_rightEar[index] = 0;
        result_surroundLeft_leftEar[index] = 0;
        result_surroundLeft_rightEar[index] = 0;
        result_surroundRight_leftEar[index] = 0;
        result_surroundRight_rightEar[index] = 0;
    }
    #else
    for( size_t index = 0; index < buffersize; index++ ){
        result_leftEar[index] = 0;
        result_rightEar[index] = 0;
    }
    #endif // ONLY_DO_STEREO
}

void initPlans( IR* in_IR ){
    in_IR->plan_fourierBuffer = fftw_plan_dft_1d( in_IR->size_ExtendedArrays, in_IR->extendedBuffer_time, in_IR->extendedBuffer_image, FFTW_FORWARD, FFTW_ESTIMATE );
    in_IR->plan_fromImageToTimeDomain = fftw_plan_dft_1d( in_IR->size_ExtendedArrays, in_IR->mult_result_image, in_IR->mult_result_time, FFTW_BACKWARD, FFTW_ESTIMATE );
    in_IR->plan_fourierIR = fftw_plan_dft_1d( in_IR->size_ExtendedArrays, in_IR->IR_samples_complex, in_IR->IR_image, FFTW_FORWARD, FFTW_ESTIMATE );
}

void multiplyComplex_subFunction( IR* in_IR, int in_startingFrom, uint32_t in_doUntil ){
    // ( a + bi ) * ( c + di ) = a*c + (a*d)i + (b*c)i - b*d
    //-> real = (a*c) - (b*d)
    //-> imag = (a*d) + (b*c)
    double a = -1;
    double b = -1;
    double c = -1;
    double d = -1;

    for( size_t index = in_startingFrom; index < in_doUntil; index++ ){
        a = in_IR->IR_image[index][0];
        b = in_IR->IR_image[index][1];
        c = in_IR->extendedBuffer_image[index][0];
        d = in_IR->extendedBuffer_image[index][1];

        in_IR->mult_result_image[index][0] = (a*c) - (b*d);
        in_IR->mult_result_image[index][1] = (a*d) + (b*c);
    }
}

void multiplyComplex( IR* in_IR ){

    // Do it in 4 threads
    #if NUMBER_OF_MULTIPLYING_THREADS == 4
    thread thread_0( multiplyComplex_subFunction, in_IR, 0, int(in_IR->size_ExtendedArrays/4) );
    thread thread_1( multiplyComplex_subFunction, in_IR, int(in_IR->size_ExtendedArrays/4), int(in_IR->size_ExtendedArrays/2) );
    thread thread_2( multiplyComplex_subFunction, in_IR, int(in_IR->size_ExtendedArrays/2), int(3*(in_IR->size_ExtendedArrays/4)) );
    thread thread_3( multiplyComplex_subFunction, in_IR, int(3*(in_IR->size_ExtendedArrays/4)), in_IR->size_ExtendedArrays );

    thread_0.join();
    thread_1.join();
    thread_2.join();
    thread_3.join();
    #endif // NUMBER_OF_MULTIPLYING_THREADS

    // Do it in 2 threads
    #if NUMBER_OF_MULTIPLYING_THREADS == 2
    thread thread_0( multiplyComplex_subFunction, in_IR, 0, int((in_IR->size_ExtendedArrays)/2) );
    thread thread_1( multiplyComplex_subFunction, in_IR, int((in_IR->size_ExtendedArrays)/2), int((in_IR->size_ExtendedArrays)) );

    thread_0.join();
    thread_1.join();
    #endif // NUMBER_OF_MULTIPLYING_THREADS

    // Do it in one thread
    #if NUMBER_OF_MULTIPLYING_THREADS == 1
    multiplyComplex_subFunction( in_IR, 0, in_IR->size_ExtendedArrays );
    #endif // NUMBER_OF_MULTIPLYING_THREADS

}

void resultBackToTimeDomain( IR* in_IR ){
    fftw_execute(in_IR->plan_fromImageToTimeDomain);
    for( size_t index = 0; index < in_IR->size_ExtendedArrays; index++ ){
        // since we are doing a multiplication of two not-normalized arrays, i need to scale it down.
        // by dynamic scaling we always get the maximum valume and avoid clipping in a long run.
        // the scalingFactor will be decreased after clipping-detection at end of processCallbackFunction()
        in_IR->mult_result_time[index][0] = in_IR->mult_result_time[index][0] * (float)( 1/(float)in_IR->size_ExtendedArrays) * ( scalingFactor - headroom );
    }
}

// I put those next two function extra because i can do multi-threading then
void AddArraysLeft( float* in_output_buffer_left ){
    for( size_t index = 0; index < buffersize; index++ ){
        #if ONLY_DO_STEREO == false
        in_output_buffer_left[index] = result_center_leftEar[index] + result_frontLeft_leftEar[index] + result_frontRight_leftEar[index] + result_surroundLeft_leftEar[index] + result_surroundRight_leftEar[index];
        #else
        in_output_buffer_left[index] = result_leftEar[index];
        #endif // ONLY_DO_STEREO
    }
}

void AddArraysRight( float* in_output_buffer_right ){
    for( size_t index = 0; index < buffersize; index++ ){
        #if ONLY_DO_STEREO == false
        in_output_buffer_right[index] = result_center_rightEar[index] + result_frontLeft_rightEar[index] + result_frontRight_rightEar[index] + result_surroundLeft_rightEar[index] + result_surroundRight_rightEar[index];
        #else
        in_output_buffer_right[index] = result_rightEar[index];
        #endif // ONLY_DO_STEREO
    }
}

void calc( const float* in_inputBuffer_time, IR* in_IR, float* in_outputArray, string in_message ){

    // copying the incomming buffer into the extendedBuffer
    for( size_t index = 0; index < in_IR->size_ExtendedArrays; index++ ){
        in_IR->extendedBuffer_time[index][0] = in_inputBuffer_time[index];
    }

    fftw_execute( in_IR->plan_fourierBuffer );
    multiplyComplex( in_IR );   // performing: mult_result_image = IR_image * buffer_image. Also: scaling

    resultBackToTimeDomain( in_IR );

    // Time to add the values from the toAddBuffer
    for( size_t index = 0; index < in_IR->size_toAddBuffer; index++ ){
        in_IR->mult_result_time[index][0] = in_IR->mult_result_time[index][0] + in_IR->toAddBuffer[index];
    }

    // Cut out the values which are relevant RIGHT NOW!
    for(size_t index = 0; index < buffersize; index++){
        in_outputArray[index] = (float)(in_IR->mult_result_time[index][0]);
    }

    // Now we have to save the currently not-relevant values to the toAddBuffer
    for( size_t index = 0; index < in_IR->size_toAddBuffer; index++ ){
        in_IR->toAddBuffer[index] = in_IR->mult_result_time[ index + buffersize ][0];
    }
}

// The process callback for this JACK application. It is called by JACK at the appropriate times.
int processCallbackFunction(uint32_t in_nframes, void *in_arg){

    //input samples
    #if ONLY_DO_STEREO == false
    float* input_buffer_center = (float*) jack_port_get_buffer(input_center, buffersize);
    float* input_buffer_frontLeft = (float*) jack_port_get_buffer(input_frontLeft, buffersize);
    float* input_buffer_frontRight = (float*) jack_port_get_buffer(input_frontRight, buffersize);
    float* input_buffer_surroundLeft = (float*) jack_port_get_buffer(input_surroundLeft, buffersize);
    float* input_buffer_surroundRight = (float*) jack_port_get_buffer(input_surroundRight, buffersize);
    #else
    float* input_buffer_left = (float*) jack_port_get_buffer( input_left, buffersize );
    float* input_buffer_right = (float*) jack_port_get_buffer( input_right, buffersize );
    #endif // ONLY_DO_STEREO

    //output samples
    float* output_buffer_left = (float *) jack_port_get_buffer( output_left, buffersize );
    float* output_buffer_right = (float *) jack_port_get_buffer( output_right, buffersize );

    #if ONLY_DO_STEREO == false
    #if MASSIVE_MULTITHREADING == true
    thread thread_center_leftEar( calc, input_buffer_center, &IRcenter_leftEar, result_center_leftEar, "center_leftEar" );
    #else
    calc( input_buffer_center, &IRcenter_leftEar, result_center_leftEar, "center_leftEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_center_rightEar( calc, input_buffer_center, &IRcenter_rightEar, result_center_rightEar, "center_rightEar" );
    #else
    calc( input_buffer_center, &IRcenter_rightEar, result_center_rightEar, "center_rightEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_frontLeft_leftEar( calc, input_buffer_frontLeft, &IRfrontLeft_leftEar, result_frontLeft_leftEar, "frontLeft_leftEar" );
    #else
    calc( input_buffer_frontLeft, &IRfrontLeft_leftEar, result_frontLeft_leftEar, "frontLeft_leftEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_frontLeft_rightEar( calc, input_buffer_frontLeft, &IRfrontLeft_rightEar, result_frontLeft_rightEar, "frontLeft_rightEar" );
    #else
    calc( input_buffer_frontLeft, &IRfrontLeft_rightEar, result_frontLeft_rightEar, "frontLeft_rightEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_frontRight_leftEar( calc, input_buffer_frontRight, &IRfrontRight_leftEar, result_frontRight_leftEar, "frontRight_leftEar" );
    #else
    calc( input_buffer_frontRight, &IRfrontRight_leftEar, result_frontRight_leftEar, "frontRight_leftEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_frontRight_rightEar( calc, input_buffer_frontRight, &IRfrontRight_rightEar, result_frontRight_rightEar, "frontRight_rightEar" );
    #else
    calc( input_buffer_frontRight, &IRfrontRight_rightEar, result_frontRight_rightEar, "frontRight_rightEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_surroundLeft_leftEar( calc, input_buffer_surroundLeft, &IRsurroundLeft_leftEar, result_surroundLeft_leftEar, "surroundLeft_leftEar" );
    #else
    calc( input_buffer_surroundLeft, &IRsurroundLeft_leftEar, result_surroundLeft_leftEar, "surroundLeft_leftEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_surroundLeft_rightEar( calc, input_buffer_surroundLeft, &IRsurroundLeft_rightEar, result_surroundLeft_rightEar, "surroundLeft_rightEar" );
    #else
    calc( input_buffer_surroundLeft, &IRsurroundLeft_rightEar, result_surroundLeft_rightEar, "surroundLeft_rightEar" );
    #endif // MASSIVE_MULTITHREADING()

    #if MASSIVE_MULTITHREADING == true
    thread thread_surroundRight_leftEar( calc, input_buffer_surroundRight, &IRsurroundRight_leftEar, result_surroundRight_leftEar, "surroundRight_leftEar" );
    #else
    calc( input_buffer_surroundRight, &IRsurroundRight_leftEar, result_surroundRight_leftEar, "surroundRight_leftEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_surroundRight_rightEar( calc, input_buffer_surroundRight, &IRsurroundRight_rightEar, result_surroundRight_rightEar, "surroundRight_rightEar" );
    #else
    calc( input_buffer_surroundRight, &IRsurroundRight_rightEar, result_surroundRight_rightEar, "surroundRight_rightEar" );
    #endif // MASSIVE_MULTITHREADING
    #else

    #if MASSIVE_MULTITHREADING == true
    thread thread_left( calc, input_buffer_left, &IR_leftEar, result_leftEar, "leftEar" );
    #else
    calc( input_buffer_left, &IR_leftEar, result_leftEar, "leftEar" );
    #endif // MASSIVE_MULTITHREADING

    #if MASSIVE_MULTITHREADING == true
    thread thread_right( calc, input_buffer_right, &IR_rightEar, result_rightEar, "rightEar" );
    #else
    calc( input_buffer_right, &IR_rightEar, result_rightEar, "rightEar" );
    #endif // MASSIVE_MULTITHREADING

    #endif // ONLY_DO_STEREO

    #if MASSIVE_MULTITHREADING
    #if ONLY_DO_STEREO == false
    thread_center_leftEar.join();
    thread_center_rightEar.join();
    thread_frontLeft_leftEar.join();
    thread_frontLeft_rightEar.join();
    thread_frontRight_leftEar.join();
    thread_frontRight_rightEar.join();
    thread_surroundLeft_leftEar.join();
    thread_surroundLeft_rightEar.join();
    thread_surroundRight_leftEar.join();
    thread_surroundRight_rightEar.join();
    #else
    thread_left.join();
    thread_right.join();
    #endif // ONLY_DO_STEREO
    #endif // MASSIVE_MULTITHREADING

    // adding them up
    #if ADD_FINALLY_ADD_ARRAYS_IN_THREADS
    thread addThread_left( AddArraysLeft, output_buffer_left );
    thread addThread_right( AddArraysRight, output_buffer_right );
    addThread_left.join();
    addThread_right.join();
    #else
    AddArraysLeft( output_buffer_left );
    AddArraysRight( output_buffer_right );
    #endif // ADD_FINALLY_ADD_ARRAYS_IN_THREADS

    // dynamic scalling...
    for( size_t index = 0; index < buffersize; index++ ){
        if(
            output_buffer_left[index] > 1 ||
            output_buffer_left[index] < -1 ||
            output_buffer_right[index] > 1 ||
            output_buffer_right[index] < -1
                                                ){
            scalingFactor = scalingFactor - delta_ScalingFactor;
            cout << "Overflow at output. setting scalingFactor to: " << scalingFactor << endl;
        }
    }
    return 0;
}

int main(){

    /*** init as jack-client ***/
    jack_client_t* client;
    const char** ports;

    if( (client = jack_client_new("jack_app")) == 0 ){                                      // try to become a client of the JACK server
        fprintf (stderr, "Error. Jack-server not running?\n");
        return 1;
    }
    jack_set_process_callback (client, processCallbackFunction, 0);                         // tell the JACK server to call 'processCallbackFunction()' whenever there is work to be done.
    buffersize = jack_get_buffer_size(client);                                              // that might be a problem. we assume the buffersize to be constant.
    jack_set_buffer_size_callback(client, (JackBufferSizeCallback)buffsize_callback, 0);    // setting up function 'buffsize_callback()' to get called if buffersize changes (which is not provided right now)
    jack_set_xrun_callback( client, (JackXRunCallback)xrun_callback, NULL );
    jack_on_shutdown (client, jack_shutdown, 0);                                            // tell the JACK server to call 'jack_shutdown()' if it ever shuts down, either entirely, or if it just decides to stop calling us.

    /*** preparing data ***/
    generateSoundfileHandles();                                                             // checking for '.wav'-files in 'IRs'-folder and generate all soundfile-handles and assign them to the IR-structs

    #if ONLY_DO_STEREO == false
    IRcenter_leftEar.size_ExtendedArrays = buffersize + IRcenter_leftEar.info.frames - 1;
    IRcenter_rightEar.size_ExtendedArrays = buffersize + IRcenter_rightEar.info.frames - 1;
    IRfrontLeft_leftEar.size_ExtendedArrays = buffersize + IRfrontLeft_leftEar.info.frames - 1;
    IRfrontLeft_rightEar.size_ExtendedArrays = buffersize + IRfrontLeft_rightEar.info.frames - 1;
    IRfrontRight_leftEar.size_ExtendedArrays = buffersize + IRfrontRight_leftEar.info.frames - 1;
    IRfrontRight_rightEar.size_ExtendedArrays = buffersize + IRfrontRight_rightEar.info.frames - 1;
    IRsurroundLeft_leftEar.size_ExtendedArrays = buffersize + IRsurroundLeft_leftEar.info.frames - 1;
    IRsurroundLeft_rightEar.size_ExtendedArrays = buffersize + IRsurroundLeft_rightEar.info.frames - 1;
    IRsurroundRight_leftEar.size_ExtendedArrays = buffersize + IRsurroundRight_leftEar.info.frames - 1;
    IRsurroundRight_rightEar.size_ExtendedArrays = buffersize + IRsurroundRight_rightEar.info.frames - 1;
    #else
    IR_leftEar.size_ExtendedArrays = buffersize + IR_leftEar.info.frames - 1;
    IR_rightEar.size_ExtendedArrays = buffersize + IR_rightEar.info.frames - 1;
    #endif // ONLY_DO_STEREO

    #if ONLY_DO_STEREO == false
    initArrays( &IRcenter_leftEar );                                                        //initiates arrays and reads soundfiles in IR->samples
    initArrays( &IRcenter_rightEar );
    initArrays( &IRfrontLeft_leftEar );
    initArrays( &IRfrontLeft_rightEar );
    initArrays( &IRfrontRight_leftEar );
    initArrays( &IRfrontRight_rightEar );
    initArrays( &IRsurroundLeft_leftEar );
    initArrays( &IRsurroundLeft_rightEar );
    initArrays( &IRsurroundRight_leftEar );
    initArrays( &IRsurroundRight_rightEar );
    #else
    initArrays( &IR_leftEar );
    initArrays( &IR_rightEar );
    #endif // ONLY_DO_STEREO

    init_resultArrays();

    #if ONLY_DO_STEREO == false
    initPlans( &IRcenter_leftEar );                                                         //initiates plans
    initPlans( &IRcenter_rightEar );
    initPlans( &IRfrontLeft_leftEar );
    initPlans( &IRfrontLeft_rightEar );
    initPlans( &IRfrontRight_leftEar );
    initPlans( &IRfrontRight_rightEar );
    initPlans( &IRsurroundLeft_leftEar );
    initPlans( &IRsurroundLeft_rightEar );
    initPlans( &IRsurroundRight_leftEar );
    initPlans( &IRsurroundRight_rightEar );
    #else
    initPlans( &IR_leftEar );
    initPlans( &IR_rightEar );
    #endif // ONLY_DO_STEREO

    #if ONLY_DO_STEREO == false
    fourierIR( &IRcenter_leftEar );                                                         // moving all IRs to image-domain
    fourierIR( &IRcenter_rightEar );
    fourierIR( &IRfrontLeft_leftEar );
    fourierIR( &IRfrontLeft_rightEar );
    fourierIR( &IRfrontRight_leftEar );
    fourierIR( &IRfrontRight_rightEar );
    fourierIR( &IRsurroundLeft_leftEar );
    fourierIR( &IRsurroundLeft_rightEar );
    fourierIR( &IRsurroundRight_leftEar );
    fourierIR( &IRsurroundRight_rightEar );
    #else
    fourierIR( &IR_leftEar );
    fourierIR( &IR_rightEar );
    #endif // ONLY_DO_STEREO

    cout << "Engine sample rate: " << jack_get_sample_rate( client ) << endl;

    /*** create ports ***/
    #if ONLY_DO_STEREO == false
    input_frontLeft = jack_port_register (client, "input_frontLeft", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    input_frontRight = jack_port_register (client, "input_frontRight", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    input_center = jack_port_register (client, "input_center", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    input_surroundLeft = jack_port_register (client, "input_surroundLeft", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    input_surroundRight = jack_port_register (client, "input_surroundRight", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);

    #else
    cout << "Calling jack_port_register() for input_left" << endl;
    input_left = jack_port_register (client, "input_left", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    cout << "Calling jack_port_register() for input_right" << endl;
    input_right = jack_port_register (client, "input_right", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    #endif // ONLY_DO_STEREO
    output_left = jack_port_register (client, "output_left", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    output_right = jack_port_register (client, "output_right", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);

    // Tell the jack-server that we are ready to rumble
    if ( jack_activate(client) ) {
        fprintf (stderr, "cannot activate client");
        return 1;
    }

    /*** connect the ports ***/
    // Note: you can't do this before the client is activated, because we can't allow
    // connections to be made to clients that aren't running.
    cout << "Scanning ports" << endl;
    ports = jack_get_ports(client, NULL, NULL, JackPortIsPhysical|JackPortIsInput);
    if( ports == NULL ){
        fprintf(stderr, "Cannot find any physical playback ports\n");
        exit(1);
    }

    if( jack_connect( client, jack_port_name(output_left), ports[0] ) ){
        fprintf (stderr, "Cannot connect output_left ports\n");
    }
    if( jack_connect( client, jack_port_name(output_right), ports[1] ) ){
        fprintf (stderr, "Cannot connect output_right ports\n");
    }

    free( ports );
    cout << "Waiting. Press return to exit!" << endl;
    getchar();
    jack_client_close (client);
    cout << "Exit now!!!" << endl;
    return 0;
}
